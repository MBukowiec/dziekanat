//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace studia22.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Oceny
    {
        public int id_oceny { get; set; }
        public Nullable<int> ocena { get; set; }
        public string ocena_słowna { get; set; }
        public Nullable<System.DateTime> data_wystawienia { get; set; }
        public string nazwa_przedmiotu { get; set; }
        public string nazwisko_prowadzacego { get; set; }
    }
}
