﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace studia22.Models
{
    public class Studencis2Controller : Controller
    {
        private BazaDziekanatEntities db = new BazaDziekanatEntities();

        // GET: Studencis2
        public ActionResult Index()
        {
            return View(db.Studenci.ToList());
        }

        // GET: Studencis2/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // GET: Studencis2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Studencis2/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "nr_albumu,PESEL,imie,nazwisko,data_urodzenia,miejscowosc,kierunek_studiow")] Studenci studenci)
        {
            if (ModelState.IsValid)
            {
                db.Studenci.Add(studenci);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(studenci);
        }

        // GET: Studencis2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // POST: Studencis2/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "nr_albumu,PESEL,imie,nazwisko,data_urodzenia,miejscowosc,kierunek_studiow")] Studenci studenci)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studenci).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(studenci);
        }

        // GET: Studencis2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // POST: Studencis2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Studenci studenci = db.Studenci.Find(id);
            db.Studenci.Remove(studenci);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
