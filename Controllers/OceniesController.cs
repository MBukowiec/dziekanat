﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using studia22.Models;

namespace studia22.Controllers
{
    public class OceniesController : Controller
    {
        private BazaDziekanatEntities db = new BazaDziekanatEntities();

        // GET: Ocenies
        public ActionResult Index()
        {
            return View(db.Oceny.ToList());
        }

        // GET: Ocenies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            return View(oceny);
        }

        // GET: Ocenies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ocenies/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_oceny,ocena,ocena_słowna,data_wystawienia,nazwa_przedmiotu,nazwisko_prowadzacego")] Oceny oceny)
        {
            if (ModelState.IsValid)
            {
                db.Oceny.Add(oceny);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oceny);
        }

        // GET: Ocenies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            return View(oceny);
        }

        // POST: Ocenies/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_oceny,ocena,ocena_słowna,data_wystawienia,nazwa_przedmiotu,nazwisko_prowadzacego")] Oceny oceny)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oceny).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oceny);
        }

        // GET: Ocenies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            return View(oceny);
        }

        // POST: Ocenies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oceny oceny = db.Oceny.Find(id);
            db.Oceny.Remove(oceny);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
